import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('Homepage/page/user login on application'), [:], FailureHandling.STOP_ON_FAILURE) 
 
WebUI.callTestCase(findTestCase('Homepage/page/user click transaksi button'), [:], FailureHandling.STOP_ON_FAILURE) 
 
WebUI.callTestCase(findTestCase('Homepage/page/user click add produk button'), [:], FailureHandling.STOP_ON_FAILURE) 
 
WebUI.callTestCase(findTestCase('Homepage/page/user click notifikasi button'), [:], FailureHandling.STOP_ON_FAILURE) 
 
WebUI.callTestCase(findTestCase('Homepage/page/user click beranda button'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Homepage/page/user click elektronik category button'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Homepage/page/user click komputer category button'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Homepage/page/user click handphone category button'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Homepage/page/user click semua category button'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Homepage/page/user search produk1'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Homepage/page/user search produk2'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Homepage/page/user search produk3'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Homepage/page/user search produk4'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Homepage/page/user search produk5'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Homepage/page/user success use homepage feature'), [:], FailureHandling.STOP_ON_FAILURE)

