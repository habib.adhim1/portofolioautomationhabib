<?xml version="1.0" encoding="UTF-8"?>
<MobileElementEntity>
   <description></description>
   <name>fieldKataSandiLama</name>
   <tag></tag>
   <elementGuidId>00000000-0000-0000-0000-000000000000</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <smartLocatorEnabled>false</smartLocatorEnabled>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>android.widget.EditText</value>
      <webElementGuid>80e89e96-5756-45b4-a7f1-f6b50836d1be</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>index</name>
      <type>Main</type>
      <value>0</value>
      <webElementGuid>0aee396c-c040-40fb-b564-64110f344c38</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Kata Sandi Lama</value>
      <webElementGuid>36b66ed4-0cd1-464b-83cc-e7db1fc28511</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>resource-id</name>
      <type>Main</type>
      <value>id.binar.fp.secondhand:id/et_curr_password</value>
      <webElementGuid>1a8ff935-dfb9-4057-b97c-4e7b3c4545d9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>package</name>
      <type>Main</type>
      <value>id.binar.fp.secondhand</value>
      <webElementGuid>34629b5f-4532-44cb-9b42-41f6125c9b3e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>checkable</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>5d1626ec-bccf-4d71-b538-d4b30c05d304</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>checked</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>b3c76d6b-8a48-4015-9456-eae82345ce91</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>clickable</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>c0cfe1e6-6e78-44cc-8adc-a380a4aac97d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>enabled</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>fc4fc0c5-e296-4798-91ab-562fcefe6ae2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>focusable</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>0234dc2f-e10b-455c-85c4-28ebedbb6052</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>focused</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>91031b6e-7fd4-4b0a-93e9-3fc13812da00</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>scrollable</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>4e293af1-ed6e-4a6b-aa82-06bf92716add</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>long-clickable</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>59536d31-c45d-4955-9254-758c9a1c8313</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>password</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>a0af13f5-0ef9-4b6c-aed1-3cf2433954e0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>selected</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>0aef6ac2-445e-4f46-bcc2-e0bbb247ffb1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>x</name>
      <type>Main</type>
      <value>63</value>
      <webElementGuid>b0297154-efb0-4500-927d-5a16acf0c971</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>y</name>
      <type>Main</type>
      <value>1451</value>
      <webElementGuid>2a7af842-4c99-4f72-a510-01c9d25bffbf</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>width</name>
      <type>Main</type>
      <value>954</value>
      <webElementGuid>add4703f-bbaf-4181-8f15-c138b758387b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>height</name>
      <type>Main</type>
      <value>136</value>
      <webElementGuid>c6ca4de0-7f94-44ef-9cae-25a9e2ac3848</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>bounds</name>
      <type>Main</type>
      <value>[63,1451][1017,1587]</value>
      <webElementGuid>119cc16b-fa73-4cef-8c77-fbaed21e0261</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>displayed</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>f009da51-ae6a-400f-aa17-b55d8225339c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>hint</name>
      <type>Main</type>
      <value>Kata Sandi Lama</value>
      <webElementGuid>89bffc46-0fab-42f9-9ef2-0d0cde9b2a9a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//hierarchy/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.view.ViewGroup[1]/android.widget.FrameLayout[1]/android.view.ViewGroup[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.EditText[1]</value>
      <webElementGuid>7cd6f4e4-7a00-4f75-a4ba-ab6a16288fbd</webElementGuid>
   </webElementProperties>
   <locator>//*[@class = 'android.widget.EditText' and (@text = 'Kata Sandi Lama' or . = 'Kata Sandi Lama') and @resource-id = 'id.binar.fp.secondhand:id/et_curr_password']</locator>
   <locatorStrategy>ATTRIBUTES</locatorStrategy>
</MobileElementEntity>
