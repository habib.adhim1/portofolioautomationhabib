@Login
Feature: Login Apps

  @LGB001
  Scenario: I want Login as Buyer user
    Given user open application buyer
    And user landing on login page buyer
    And user input username buyer
    And user input password buyer
    Then user success login as buyer

  @LGS002
  Scenario: I want Login as Seller user
    Given user open application seller
    And user landing on login page seller
    And user input username seller
    And user input password seller
    Then user success login as seller
