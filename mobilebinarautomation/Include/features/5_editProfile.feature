@editProduct
Feature: editProduct


	@PR001
	Scenario: User update their profile (picture from gallery)
		Given user landing on profile page
		And user click pencil icon
		And user inout profile picture with gallery
		And user success edit
		And user edit name
		And user success edit
		And user edit phone number
		And user success edit
		And user edit kota
		And user success edit
		And user edit alamat
		And user success edit
		And user edit email
		And user success edit
		And user edit password
		Then user success edit
		
	@PR002
	Scenario: User update their profile (picture from camera)
	
		Given user landing on profile page
		And user click pencil icon
		And user inout profile picture with camera
		And user success edit
		And user edit name
		And user success edit
		And user edit phone number
		And user success edit
		And user edit kota
		And user success edit
		And user edit alamat
		And user success edit
		And user edit email
		And user success edit
		And user edit password
		Then user success edit