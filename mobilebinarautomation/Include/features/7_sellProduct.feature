@sellProduct
Feature: sellProduct


	@SE001
	Scenario: User sell a product (picture from camera)
		Given user landing on sellpage
		And user input product name
		And user input product price
		And user input product category
		And user input lokasi
		And user input product description
		And user input picture with camera
		And user click preview
		And user click terbitkan
		Then user success create a product

	@SE002
	Scenario: User sell a product (picture from gallery)
		Given user landing on sellpage
		And user input product name
		And user input product price
		And user input product category
		And user input lokasi
		And user input product description
		And user input product picture with gallery
		And user click preview
		And user click terbitkan
		Then user success create a product