@EditProduk
Feature: Edit Produk

@EP001
Scenario: as a user i want to edit product info (image from camera)
Given user landing on produk page
When user click produk edit
And user input produk name
And user input produk price
And user click dropdown category
And user input location
And user input deskripsi
And user upload product foto
And user click perbarui product
Then user success edit product

@EP002
Scenario: as a user i want to edit product info (image from gallery)
Given user landing on produk page
When user click produk edit
And user input produk name
And user input produk price
And user click dropdown category
And user input location
And user input deskripsi
And user upload from gallery
And user click perbarui product
Then user success edit product
