package stepDefinition
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class editProduct {
	@Given ("user landing on produk page")
	public void user_landing_on_produk_page() {
		WebUI.callTestCase(findTestCase('Edit Produk/page/user landing on produk page'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When ("user click produk edit")
	public void user_click_produk_edit() {
		WebUI.callTestCase(findTestCase('Edit Produk/page/User click produk edit'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@And ("user input produk name")
	public void user_input_produk_name() {
		WebUI.callTestCase(findTestCase('Edit Produk/page/user input product name'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@And ("user input produk price")
	public void user_input_produk_price() {
		WebUI.callTestCase(findTestCase('Edit Produk/page/user input product price'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@And ("user click dropdown category")
	public void user_click_dropdown_category() {
		WebUI.callTestCase(findTestCase('Edit Produk/page/user click dropdown category'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@And ("user input location")
	public void user_input_location() {
		WebUI.callTestCase(findTestCase('Edit Produk/page/user input lokasi'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@And ("user input deskripsi")
	public void user_input_deskripsi() {
		WebUI.callTestCase(findTestCase('Edit Produk/page/user input description'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@And ("user upload product foto")
	public void user_upload_product_foto() {
		WebUI.callTestCase(findTestCase('Edit Produk/page/user upload product foto'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@And ("user click perbarui product")
	public void user_click_perbarui_product() {
		WebUI.callTestCase(findTestCase('Edit Produk/page/user click perbarui product'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then ("user success edit product")
	public void user_success_edit_product() {
		WebUI.callTestCase(findTestCase('Edit Produk/page/user success edit product'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	
	@And ("user upload from gallery")
	public void user_upload_from_gallery() {
		WebUI.callTestCase(findTestCase('Edit Produk/page/user upload from gallery'), [:], FailureHandling.STOP_ON_FAILURE)
	}
}