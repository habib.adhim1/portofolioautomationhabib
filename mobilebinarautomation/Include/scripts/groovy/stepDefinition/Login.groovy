package stepDefinition
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When




class Login {
	////////////////////////BUYER//////////////////////////////////
	@Given("user open application buyer")
	public void user_open_application_buyer( ) {
		WebUI.callTestCase(findTestCase('Login/page/user open application'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	@When("user landing on login page buyer")
	public void user_landing_on_login_page_buyer( ) {
		WebUI.callTestCase(findTestCase('Login/page/user landing on login page'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	@And("user input username buyer")
	public void user_input_username_buyer( ) {
		WebUI.callTestCase(findTestCase('Login/page/user input username buyer'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	@And("user input password buyer")
	public void user_input_password_buyer( ) {
		WebUI.callTestCase(findTestCase('Login/page/user input password'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	@And("user success login as buyer")
	public void user_success_login_as_buyer( ) {
		WebUI.callTestCase(findTestCase('Login/page/user success login'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	@Then("user success logout as buyer")
	public void user_success_logout_as_buyer( ) {
		WebUI.callTestCase(findTestCase('Logout/Page/user success Logout'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	////////////////////////SELLER//////////////////////////////////
	@Given("user open application seller")
	public void user_open_application_seller( ) {
		WebUI.callTestCase(findTestCase('Login/page/user open application'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	@When("user landing on login page seller")
	public void user_landing_on_login_page_seller( ) {
		WebUI.callTestCase(findTestCase('Login/page/user landing on login page'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	@And("user input username seller")
	public void user_input_username_seller( ) {
		WebUI.callTestCase(findTestCase('Login/page/user input username seller'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	@And("user input password seller")
	public void user_input_password_seller( ) {
		WebUI.callTestCase(findTestCase('Login/page/user input password'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	@And("user success login as seller")
	public void user_success_login_as_seller( ) {
		WebUI.callTestCase(findTestCase('Login/page/user success login'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	@Then("user success logout as seller")
	public void user_success_logout_as_seller( ) {
		WebUI.callTestCase(findTestCase('Logout/Page/user success Logout'), [:], FailureHandling.STOP_ON_FAILURE)
	}
}
















