package stepDefinition
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class homepage {
	@Given("user landing on application")
	public void uuser_landing_on_application() {
		WebUI.callTestCase(findTestCase('Homepage/page/user login on application'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	
	@When("user click transaksi button")
	public void user_click_transaksi_button() {
		WebUI.callTestCase(findTestCase('Homepage/page/user click transaksi button'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	
	@And("user click add produk button")
	public void user_click_add_produk_button() {
		WebUI.callTestCase(findTestCase('Homepage/page/user click add produk button'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	
	@And("user click notifikasi")
	public void user_click_notifikasi() {
		WebUI.callTestCase(findTestCase('Homepage/page/user click notifikasi button'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	
	@And("user click beranda button")
	public void user_click_beranda_button() {
		WebUI.callTestCase(findTestCase('Homepage/page/user click beranda button'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@And("user click elektronik category button")
	public void user_click_elektronik_category_button() {
		WebUI.callTestCase(findTestCase('Homepage/page/user click elektronik category button'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@And("user click komputer category button")
	public void user_click_komputer_category_button() {
		WebUI.callTestCase(findTestCase('Homepage/page/user click komputer category button'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@And("user click handphone category button")
	public void user_click_handphone_category_button() {
		WebUI.callTestCase(findTestCase('Homepage/page/user click handphone category button'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@And("user click semua category button")
	public void user_click_semua_category_button() {
		WebUI.callTestCase(findTestCase('Homepage/page/user click semua category button'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@And("user search produk1")
	public void user_search_produk1() {
		WebUI.callTestCase(findTestCase('Homepage/page/user search produk1'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@And("user search produk2")
	public void user_search_produk2() {
		WebUI.callTestCase(findTestCase('Homepage/page/user search produk2'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@And("user search produk3")
	public void user_search_produk3() {
		WebUI.callTestCase(findTestCase('Homepage/page/user search produk3'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@And("user search produk4")
	public void user_search_produk4() {
		WebUI.callTestCase(findTestCase('Homepage/page/user search produk4'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@And("user search produk5")
	public void user_search_produk5() {
		WebUI.callTestCase(findTestCase('Homepage/page/user search produk5'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("user success use homepage feature")
	public void user_success_use_homepage_feature() {
		WebUI.callTestCase(findTestCase('Homepage/page/user success use homepage feature'), [:], FailureHandling.STOP_ON_FAILURE)
	}
}