package stepDefinition
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class buyProduct {
	/**
	 * The step definitions below match with Katalon sample Gherkin steps
	 */
	@Given("user landing on homepagee")
	public void user_landing_on_homepagee( ) {
		WebUI.callTestCase(findTestCase('Buy Product/page/user landing on homepage'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("user search a product")
	public void user_click_pencil_icon( ) {
		WebUI.callTestCase(findTestCase('Buy Product/page/user search a product'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("user tap product")
	public void user_tap_product( ) {
		WebUI.callTestCase(findTestCase('Buy Product/page/user tap product'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("user tap tertarik dan ingin nego")
	public void user_tap_tertarik_dan_ingin_nego( ) {
		WebUI.callTestCase(findTestCase('Buy Product/page/user tap tertarik dan ingin nego'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("user input harga tawar")
	public void user_input_harga_tawar( ) {
		WebUI.callTestCase(findTestCase('Buy Product/page/user input harga tawar'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("user tap kirim")
	public void user_tap_kirim( ) {
		WebUI.callTestCase(findTestCase('Buy Product/page/user tap kirim'), [:], FailureHandling.STOP_ON_FAILURE)
	}
}