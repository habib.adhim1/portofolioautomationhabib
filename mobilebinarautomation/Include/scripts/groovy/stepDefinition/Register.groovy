package stepDefinition
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When




class Register {

	@Given("user open application")
	public void user_open_application( ) {
		WebUI.callTestCase(findTestCase('Login/page/user open application'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	@When("user landing on login page")
	public void user_landing_on_login_page( ) {
		WebUI.callTestCase(findTestCase('Login/page/user landing on login page'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	@And("user click button Daftar")
	public void user_click_button_Daftar( ) {
		WebUI.callTestCase(findTestCase('Register/page/user click button daftar'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	@And("user input Nama")
	public void user_input_Nama( ) {
		WebUI.callTestCase(findTestCase('Register/page/user input Nama field'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	@And("user input Email")
	public void user_input_Email( ) {
		WebUI.callTestCase(findTestCase('Register/page/user input Email field'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	@And("user input Password")
	public void user_input_Password( ) {
		WebUI.callTestCase(findTestCase('Register/page/user input Password field'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	@And("user input Nomor HP")
	public void user_input_Nomor_HP( ) {
		WebUI.callTestCase(findTestCase('Register/page/user input Nomor HP field'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	@And("user input Kota")
	public void user_input_Kota( ) {
		WebUI.callTestCase(findTestCase('Register/page/user input Kota field'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	@And("user input Alamat")
	public void user_input_Alamat( ) {
		WebUI.callTestCase(findTestCase('Register/page/user input Alamat field'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	@Then("user success Register New Account")
	public void user_success_Register_New_Account( ) {
		WebUI.callTestCase(findTestCase('Register/page/user success Register account'), [:], FailureHandling.STOP_ON_FAILURE)
	}
}





































