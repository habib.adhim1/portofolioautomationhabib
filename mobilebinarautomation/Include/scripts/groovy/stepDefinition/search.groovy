package stepDefinition
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import java.security.PublicKey

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class search {
	@Given("user open app and landing on homepage")
	public void user_open_app_and_landing_on_homepage() {
		WebUI.callTestCase(findTestCase('Search/page/user open app and landing on homepage'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	
	@When("user input keyword for search")
	public void user_input_keyword_for_search() {
		WebUI.callTestCase(findTestCase('Search/page/user input keyword for search'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	
	@And("user click produk search1")
	public void user_click_produk_search1() {
		WebUI.callTestCase(findTestCase('Search/page/user click produk search1'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	
	@And("user click produk search2")
	public void user_click_produk_search2() {
		WebUI.callTestCase(findTestCase('Search/page/user click produk search2'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	
	@And("user click produk search3")
	public void user_click_produk_search3() {
		WebUI.callTestCase(findTestCase('Search/page/user click produk search3'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	@And("user click produk search4")
	public void user_click_produk_search4() {
		WebUI.callTestCase(findTestCase('Search/page/user click produk search4'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	
	@And("user click produk search5")
	public void user_click_produk_search5() {
		WebUI.callTestCase(findTestCase('Search/page/user click produk search5'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	
	@Then("user success use search feature")
	public void user_success_use_search_feature() {
		WebUI.callTestCase(findTestCase('Search/page/user success use search feature'), [:], FailureHandling.STOP_ON_FAILURE)
	}
}