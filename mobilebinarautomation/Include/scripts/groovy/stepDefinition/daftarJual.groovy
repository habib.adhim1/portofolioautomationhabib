package stepDefinition
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class daftarJual {
	@Given ("user landing on akun page")
	public void user_landing_on_akun_page() {
		WebUI.callTestCase(findTestCase('Daftar Jual/page/user landing on akun page'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When ("user click daftar jual button")
	public void user_click_daftar_jual_button() {
		WebUI.callTestCase(findTestCase('Daftar Jual/page/user click daftar jual button'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@And ("user click produk grup2")
	public void user_click_produk_grup2() {
		WebUI.callTestCase(findTestCase('Daftar Jual/page/user click produk grup2'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@And ("user click diminati button")
	public void user_click_diminati_button() {
		WebUI.callTestCase(findTestCase('Daftar Jual/page/user click diminati button'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@And ("user click terjual button")
	public void user_click_terjual_button() {
		WebUI.callTestCase(findTestCase('Daftar Jual/page/user click terjual button'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@And ("user click produk button")
	public void user_click_produk_button() {
		WebUI.callTestCase(findTestCase('Daftar Jual/page/user click produk button'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then ("user success use daftar jual feature")
	public void user_success_use_daftar_jual_feature() {
		WebUI.callTestCase(findTestCase('Daftar Jual/page/user success use daftar jual feature'), [:], FailureHandling.STOP_ON_FAILURE)
	}
}