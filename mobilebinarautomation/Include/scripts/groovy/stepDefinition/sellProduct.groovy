package stepDefinition
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class sellProduct {
	/**
	 * The step definitions below match with Katalon sample Gherkin steps
	 */
	@Given("user landing on sellpage")
	public void user_landing_on_sellpage( ) {
		WebUI.callTestCase(findTestCase('Sell Product/page/user landing on sellpage'), [:], FailureHandling.STOP_ON_FAILURE)
	}


	@Then("user input product name")
	public void user_input_product_name( ) {
		WebUI.callTestCase(findTestCase('Sell Product/page/user input product name'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("user input product price")
	public void user_input_product_price( ) {
		WebUI.callTestCase(findTestCase('Sell Product/page/user input product price'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("user input product category")
	public void user_input_product_category( ) {
		WebUI.callTestCase(findTestCase('Sell Product/page/user input product category'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("user input lokasi")
	public void user_input_lokasi( ) {
		WebUI.callTestCase(findTestCase('Sell Product/page/user input lokasi'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("user input product description")
	public void user_input_product_description( ) {
		WebUI.callTestCase(findTestCase('Sell Product/page/user input product description'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("user input picture with camera")
	public void user_input_picture_with_camera( ) {
		WebUI.callTestCase(findTestCase('Sell Product/page/user input product picture with camera'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("user click preview")
	public void user_click_preview( ) {
		WebUI.callTestCase(findTestCase('Sell Product/page/user click preview'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("user click terbitkan")
	public void user_click_terbitkan( ) {
		WebUI.callTestCase(findTestCase('Sell Product/page/user click terbitkan'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("user success create a product")
	public void user_success_create_a_product( ) {
		WebUI.callTestCase(findTestCase('Sell Product/page/user success create a product'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("user input product picture with gallery")
	public void user_input_product_picture_with_gallery( ) {
		WebUI.callTestCase(findTestCase('Sell Product/page/user input product picture with gallery'), [:], FailureHandling.STOP_ON_FAILURE)
	}
}