package stepDefinition
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class editProfile {
	/**
	 * The step definitions below match with Katalon sample Gherkin steps
	 */
	@Given("user landing on profile page")
	public void user_landing_on_profile_page( ) {
		WebUI.callTestCase(findTestCase('Profile/page/user landing on profile page'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("user click pencil icon")
	public void user_click_pencil_icon( ) {
		WebUI.callTestCase(findTestCase('Profile/page/user click pencil icon'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("user inout profile picture with gallery")
	public void user_input_profile_picture_with_gallery( ) {
		WebUI.callTestCase(findTestCase('Profile/page/user input profile picture with gallery'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("user success edit")
	public void user_success_edit( ) {
		WebUI.callTestCase(findTestCase('Profile/page/user success edit'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("user edit name")
	public void user_edit_name( ) {
		WebUI.callTestCase(findTestCase('Profile/page/user edit name'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("user edit phone number")
	public void user_edit_phone_number( ) {
		WebUI.callTestCase(findTestCase('Profile/page/user edit phone number'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("user edit kota")
	public void user_edit_kota( ) {
		WebUI.callTestCase(findTestCase('Profile/page/user edit kota'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("user edit alamat")
	public void user_edit_alamat( ) {
		WebUI.callTestCase(findTestCase('Profile/page/user edit alamat'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("user edit email")
	public void user_edit_email( ) {
		WebUI.callTestCase(findTestCase('Profile/page/user edit email'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("user inout profile picture with camera")
	public void user_input_profile_picture_with_camera( ) {
		WebUI.callTestCase(findTestCase('Profile/page/user input profile picture with camera'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	
	@Then("user edit password")
	public void user_edit_password( ) {
		WebUI.callTestCase(findTestCase('Profile/page/user edit password'), [:], FailureHandling.STOP_ON_FAILURE)
	}
}