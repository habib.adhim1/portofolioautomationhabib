import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('pageSell/Pages/User landing on sellpage'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('pageSell/Pages/User input product name'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('pageSell/Pages/User input product price'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('pageSell/Pages/User input category'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('pageSell/Pages/User input description'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('pageSell/Pages/User upload image'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('pageSell/Pages/User click preview'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('pageSell/Pages/User success go to preview page'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('pageSell/Pages/User click terbitkan'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('pageSell/Pages/User success go to preview page'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('pageSell/Pages/User successfully sell a product'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('pageHome/Pages/User click home'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('pageSell/Pages/User successfully sell a product'), [:], FailureHandling.STOP_ON_FAILURE)

