import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('pageEditProduct/Page/user login as a seller'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('pageEditProduct/Page/user click icon sandwich'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('pageEditProduct/Page/user click card product'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('pageEditProduct/Page/user click button edit'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('pageEditProduct/Page/user set nama produk'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('pageEditProduct/Page/user set harga produk'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('pageEditProduct/Page/user set dropdown kategori'), [('variable') : 'Hobi'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('pageEditProduct/Page/user set deskripsi'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('pageEditProduct/Page/user click icon delete image'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('pageEditProduct/Page/user click button preview'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('pageEditProduct/Page/user click carousel control next'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('pageEditProduct/Page/user click carousel control prev'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('pageEditProduct/Page/user click icon carousel indicator'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('pageEditProduct/Page/user click button terbitkan preview'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('pageEditProduct/Page/user click icon sandwich'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('pageEditProduct/Page/user back to Homepage'), [:], FailureHandling.STOP_ON_FAILURE)

