import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('pageNotification/testNotification/testOpenBrowser'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('pageNotification/testNotification/testNavigateToUrl'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('pageNotification/testNotification/testMaximizeWindow'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('pageNotification/testNotification/testBtnMasukLogin'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('pageLogin/testLogin/testSetEmailSeller'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('pageLogin/testLogin/testSetPassword'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('pageLogin/testLogin/testBtnLogin'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('pageNotification/testNotification/testIconNotification'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('pageNotification/testNotification/testImageNotification'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('pageNotification/testNotification/testBtnBackHome'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('pageNotification/testNotification/testUserCloseBrowse'), [:], FailureHandling.STOP_ON_FAILURE)

