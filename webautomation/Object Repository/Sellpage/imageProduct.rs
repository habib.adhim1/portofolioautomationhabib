<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>imageProduct</name>
   <tag></tag>
   <elementGuidId>4906753c-6d42-4e72-9906-01dfb6806a98</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'product_images']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <smartLocatorEnabled>false</smartLocatorEnabled>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>product_images</value>
      <webElementGuid>b3f4484f-25d5-42af-86bc-97ed3f68f8d7</webElementGuid>
   </webElementProperties>
</WebElementEntity>
