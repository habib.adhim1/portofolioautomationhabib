<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>categoryDropDown</name>
   <tag></tag>
   <elementGuidId>677aebc0-5d9e-48d1-8d1f-686e22d7a27a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'product_category_id']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <smartLocatorEnabled>false</smartLocatorEnabled>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>product_category_id</value>
      <webElementGuid>995ab5ce-8696-4a60-a724-2699b3648545</webElementGuid>
   </webElementProperties>
</WebElementEntity>
