<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>descriptionField</name>
   <tag></tag>
   <elementGuidId>d7e345c0-41e3-4089-b445-30055380be13</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'product_description']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <smartLocatorEnabled>false</smartLocatorEnabled>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>product_description</value>
      <webElementGuid>fa73587b-584d-4743-a942-d0d240005156</webElementGuid>
   </webElementProperties>
</WebElementEntity>
