<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>productPriceField</name>
   <tag></tag>
   <elementGuidId>10cda8c0-23a3-4a56-9d46-39ad11ad5cd5</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'product_price']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <smartLocatorEnabled>false</smartLocatorEnabled>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>product_price</value>
      <webElementGuid>887b008b-49aa-4e11-998e-d51394b9da08</webElementGuid>
   </webElementProperties>
</WebElementEntity>
