<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>productNameField</name>
   <tag></tag>
   <elementGuidId>93dd38e9-b782-4165-955f-67efd81d43d0</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'product_name']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <smartLocatorEnabled>false</smartLocatorEnabled>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>product_name</value>
      <webElementGuid>1360439b-be60-4b07-b2c9-09f2f2fd3f4e</webElementGuid>
   </webElementProperties>
</WebElementEntity>
