<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btnPreview</name>
   <tag></tag>
   <elementGuidId>4e96a902-dd34-450e-8f57-9be165d8a735</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//label[@class='btn btn-outline-primary w-50 rounded-4 p-3']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <smartLocatorEnabled>false</smartLocatorEnabled>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//label[@class='btn btn-outline-primary w-50 rounded-4 p-3']</value>
      <webElementGuid>628fce15-b6c7-4477-8e25-0b2ef61228ae</webElementGuid>
   </webElementProperties>
</WebElementEntity>
