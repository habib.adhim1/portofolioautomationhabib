<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>addImage</name>
   <tag></tag>
   <elementGuidId>8deb8104-4232-4c9c-bc8d-f1262c07caf2</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//div[@class='form-image rounded-4 img-preview']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <smartLocatorEnabled>false</smartLocatorEnabled>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//div[@class='form-image rounded-4 img-preview']</value>
      <webElementGuid>acfcd00f-20f9-4b96-b51a-c4efcad78e3a</webElementGuid>
   </webElementProperties>
</WebElementEntity>
