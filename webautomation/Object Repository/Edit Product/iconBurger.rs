<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>iconBurger</name>
   <tag></tag>
   <elementGuidId>1bf72235-3f9c-40bb-a46c-cf94375a6820</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//i[@class='bi bi-list-ul me-4 me-lg-0']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <smartLocatorEnabled>false</smartLocatorEnabled>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//i[@class='bi bi-list-ul me-4 me-lg-0']</value>
      <webElementGuid>19b93a2e-0c9f-417a-9222-083983dba88d</webElementGuid>
   </webElementProperties>
</WebElementEntity>
