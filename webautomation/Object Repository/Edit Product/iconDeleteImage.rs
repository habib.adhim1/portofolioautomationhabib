<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>iconDeleteImage</name>
   <tag></tag>
   <elementGuidId>369ba2e6-54ed-4120-89c5-b302c2e1e7d1</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//button[@class='btn-close btn-close-white position-absolute top-0 start-100 translate-middle']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <smartLocatorEnabled>false</smartLocatorEnabled>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//button[@class='btn-close btn-close-white position-absolute top-0 start-100 translate-middle']</value>
      <webElementGuid>4796a788-522a-43c7-b6d4-ecf8b2ce0600</webElementGuid>
   </webElementProperties>
</WebElementEntity>
