<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>cardProduct</name>
   <tag></tag>
   <elementGuidId>e22cdf23-caf2-4f58-8a5f-01bf14ffca50</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//div[5]//div[@class='card-body text-decoration-none']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <smartLocatorEnabled>false</smartLocatorEnabled>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//div[5]//div[@class='card-body text-decoration-none']</value>
      <webElementGuid>ee649bb0-93cf-4ed9-9cf1-8e875c7273c1</webElementGuid>
   </webElementProperties>
</WebElementEntity>
