<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btnTerbitkan</name>
   <tag></tag>
   <elementGuidId>217155eb-ad87-4397-bec0-8886da6eb072</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//label[@class='btn btn-primary w-50 rounded-4 p-3']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <smartLocatorEnabled>false</smartLocatorEnabled>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//label[@class='btn btn-primary w-50 rounded-4 p-3']</value>
      <webElementGuid>1bc86fd4-97eb-40e0-b27b-4a7234c872dd</webElementGuid>
   </webElementProperties>
</WebElementEntity>
