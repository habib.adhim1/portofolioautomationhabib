<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btnLogout</name>
   <tag></tag>
   <elementGuidId>e0695524-c81f-47c4-9d8a-f09318230b5e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//button[@class='btn btn-link text-decoration-none']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <smartLocatorEnabled>false</smartLocatorEnabled>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//button[@class='btn btn-link text-decoration-none']</value>
      <webElementGuid>953bc7eb-1af1-46ea-98e0-e5a836dfd681</webElementGuid>
   </webElementProperties>
</WebElementEntity>
