<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>productBaju</name>
   <tag></tag>
   <elementGuidId>fdd63ae3-3365-4ce8-ada8-be15d4ae80ed</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//a[@href='/products/113681']/div[@class='card px-0 border-0 shadow h-100 pb-4 rounded-4']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <smartLocatorEnabled>false</smartLocatorEnabled>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//a[@href='/products/113681']/div[@class='card px-0 border-0 shadow h-100 pb-4 rounded-4']</value>
      <webElementGuid>6d756f1c-b8be-49be-a38f-7d95e8ba69ad</webElementGuid>
   </webElementProperties>
</WebElementEntity>
