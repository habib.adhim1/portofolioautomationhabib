<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>productHobi</name>
   <tag></tag>
   <elementGuidId>63d44851-90a4-47fd-89b4-fd2a00b6bf33</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//a[@href='/products/113679']/div[@class='card px-0 border-0 shadow h-100 pb-4 rounded-4']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <smartLocatorEnabled>false</smartLocatorEnabled>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//a[@href='/products/113679']/div[@class='card px-0 border-0 shadow h-100 pb-4 rounded-4']</value>
      <webElementGuid>bdec8507-04e3-47eb-8010-a2e0f72b602d</webElementGuid>
   </webElementProperties>
</WebElementEntity>
