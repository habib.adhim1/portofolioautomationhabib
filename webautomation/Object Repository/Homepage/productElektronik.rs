<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>productElektronik</name>
   <tag></tag>
   <elementGuidId>bb088d38-313a-4af9-b9a0-39edf70ed95b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//a[@href='/products/113682']/div[@class='card px-0 border-0 shadow h-100 pb-4 rounded-4']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <smartLocatorEnabled>false</smartLocatorEnabled>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//a[@href='/products/113682']/div[@class='card px-0 border-0 shadow h-100 pb-4 rounded-4']</value>
      <webElementGuid>56f50b9f-e1d8-486e-b35c-d7f8ebe70d64</webElementGuid>
   </webElementProperties>
</WebElementEntity>
