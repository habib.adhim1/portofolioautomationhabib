<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btnNext</name>
   <tag></tag>
   <elementGuidId>3b89c65d-7a33-4a31-ae80-7695e633b61b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//a[.='Next →']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <smartLocatorEnabled>false</smartLocatorEnabled>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//a[.='Next →']</value>
      <webElementGuid>eefc8e2f-be3c-4fc2-a758-28e5526a02c3</webElementGuid>
   </webElementProperties>
</WebElementEntity>
