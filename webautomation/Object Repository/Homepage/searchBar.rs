<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>searchBar</name>
   <tag></tag>
   <elementGuidId>b43ddb97-eb0b-47f9-a8c7-bc2a29f98df1</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@name = 'q']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <smartLocatorEnabled>false</smartLocatorEnabled>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>q</value>
      <webElementGuid>fc245eeb-b003-452f-8287-dcb78bce0dc6</webElementGuid>
   </webElementProperties>
</WebElementEntity>
