<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>iconProfile</name>
   <tag></tag>
   <elementGuidId>37a039a2-e767-4182-b33a-a525f8efde7c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//li[@class='nav-item dropdown fs-5 d-none d-lg-block']//i[@class='bi bi-person me-4 me-lg-0']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <smartLocatorEnabled>false</smartLocatorEnabled>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//li[@class='nav-item dropdown fs-5 d-none d-lg-block']//i[@class='bi bi-person me-4 me-lg-0']</value>
      <webElementGuid>a1d12674-287e-471b-8539-d22195cdaa0f</webElementGuid>
   </webElementProperties>
</WebElementEntity>
