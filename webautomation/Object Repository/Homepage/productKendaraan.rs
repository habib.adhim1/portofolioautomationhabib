<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>productKendaraan</name>
   <tag></tag>
   <elementGuidId>ea8c964f-e266-407e-b18b-fcc3d3fe090d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//a[@href='/products/113680']/div[@class='card px-0 border-0 shadow h-100 pb-4 rounded-4']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <smartLocatorEnabled>false</smartLocatorEnabled>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//a[@href='/products/113680']/div[@class='card px-0 border-0 shadow h-100 pb-4 rounded-4']</value>
      <webElementGuid>831b3fec-bcee-4404-9556-98bda07a8781</webElementGuid>
   </webElementProperties>
</WebElementEntity>
