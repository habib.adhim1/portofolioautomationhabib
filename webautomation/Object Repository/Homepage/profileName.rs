<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>profileName</name>
   <tag></tag>
   <elementGuidId>ca338c7f-85b9-4eb0-8831-b191a93fde9b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//div[@class='d-flex align-items-center justify-content center flex-column py-2']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <smartLocatorEnabled>false</smartLocatorEnabled>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//div[@class='d-flex align-items-center justify-content center flex-column py-2']</value>
      <webElementGuid>1e785dd9-40a0-4a6c-9908-18c97e2c4dd8</webElementGuid>
   </webElementProperties>
</WebElementEntity>
