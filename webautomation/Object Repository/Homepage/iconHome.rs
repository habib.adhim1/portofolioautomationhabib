<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>iconHome</name>
   <tag></tag>
   <elementGuidId>e2efe769-edc0-4700-b7af-9c78ad01cef7</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>.navbar-brand</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <smartLocatorEnabled>false</smartLocatorEnabled>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>css</name>
      <type>Main</type>
      <value>.navbar-brand</value>
      <webElementGuid>1ee9bc77-61ed-4caf-b2a0-4e65f286b37f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//a[@href='/']</value>
      <webElementGuid>d281f278-3a89-4cbb-a930-ce23f0fa31e2</webElementGuid>
   </webElementProperties>
</WebElementEntity>
