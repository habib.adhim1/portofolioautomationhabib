<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>dropdownAccount</name>
   <tag></tag>
   <elementGuidId>2e80a0da-09e9-45c9-8273-f7b1d83006ce</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//li[@class='nav-item dropdown fs-5 d-none d-lg-block']//i[@class='bi bi-person me-4 me-lg-0']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <smartLocatorEnabled>false</smartLocatorEnabled>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//li[@class='nav-item dropdown fs-5 d-none d-lg-block']//i[@class='bi bi-person me-4 me-lg-0']</value>
      <webElementGuid>c802d6b1-fea6-4203-b57f-fa782ce5ad21</webElementGuid>
   </webElementProperties>
</WebElementEntity>
