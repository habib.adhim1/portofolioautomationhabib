<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btnUploadFotoProfile</name>
   <tag></tag>
   <elementGuidId>059727f7-9fb0-489f-84d3-ce97b9932f25</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'form-avatar']</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <smartLocatorEnabled>false</smartLocatorEnabled>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>form-avatar</value>
      <webElementGuid>894749df-f27b-4df3-8ee3-1eb2f59672da</webElementGuid>
   </webElementProperties>
</WebElementEntity>
