<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>fieldNoHP</name>
   <tag></tag>
   <elementGuidId>76887ade-fdb0-4031-8917-de37ffed12d2</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'user_phone_number']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <smartLocatorEnabled>false</smartLocatorEnabled>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>user_phone_number</value>
      <webElementGuid>1734b9cf-a670-4af0-b092-d85da2f89b37</webElementGuid>
   </webElementProperties>
</WebElementEntity>
