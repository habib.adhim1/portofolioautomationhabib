<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>setEmail</name>
   <tag></tag>
   <elementGuidId>2388e823-8e36-4580-8951-91301281dd0f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'user_email']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <smartLocatorEnabled>false</smartLocatorEnabled>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>user_email</value>
      <webElementGuid>3a42be40-311b-4400-a033-066342873442</webElementGuid>
   </webElementProperties>
</WebElementEntity>
