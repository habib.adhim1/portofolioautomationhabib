<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>iconProfile</name>
   <tag></tag>
   <elementGuidId>d2c8e726-e349-4e74-ac97-bb4dc85fc555</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//li[@class='nav-item dropdown fs-5 d-none d-lg-block']/a[contains(.,'Profil Saya')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <smartLocatorEnabled>false</smartLocatorEnabled>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//li[@class='nav-item dropdown fs-5 d-none d-lg-block']/a[contains(.,'Profil Saya')]</value>
      <webElementGuid>af0c4a38-1d80-4dc4-8373-57c61943395d</webElementGuid>
   </webElementProperties>
</WebElementEntity>
