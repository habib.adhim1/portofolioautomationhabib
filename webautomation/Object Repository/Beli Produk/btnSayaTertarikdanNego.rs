<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btnSayaTertarikdanNego</name>
   <tag></tag>
   <elementGuidId>bc7fdcfb-5d07-42ee-95c0-36993450a5fa</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//button[@class='btn btn-lg btn-primary fs-6 rounded-4 w-100 mb-3 py-3 fw-bold']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <smartLocatorEnabled>false</smartLocatorEnabled>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//button[@class='btn btn-lg btn-primary fs-6 rounded-4 w-100 mb-3 py-3 fw-bold']</value>
      <webElementGuid>e2305cb7-8ce7-4e42-9746-da54605caf16</webElementGuid>
   </webElementProperties>
</WebElementEntity>
