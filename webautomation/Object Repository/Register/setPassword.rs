<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>setPassword</name>
   <tag></tag>
   <elementGuidId>fa6c98a7-b98c-4b06-9c5f-399a5c9fdc40</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'user_password']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <smartLocatorEnabled>false</smartLocatorEnabled>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>user_password</value>
      <webElementGuid>a7db5c92-1ba5-4c00-8d57-7373c0758051</webElementGuid>
   </webElementProperties>
</WebElementEntity>
