<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>imageNotiffication</name>
   <tag></tag>
   <elementGuidId>f90b2108-a887-407d-b200-3800b0cfe6db</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//ul[@class='dropdown-menu notification-dropdown-menu px-4 show']/li[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <smartLocatorEnabled>false</smartLocatorEnabled>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//ul[@class='dropdown-menu notification-dropdown-menu px-4 show']/li[1]</value>
      <webElementGuid>4d974867-61ef-4acf-ae62-26ecb65bfbb2</webElementGuid>
   </webElementProperties>
</WebElementEntity>
