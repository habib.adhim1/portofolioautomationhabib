<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btnSemuaProduk</name>
   <tag></tag>
   <elementGuidId>3c955110-67ae-4351-a6b3-8d84cb10d7c4</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//a[contains(.,'Semua Produk')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <smartLocatorEnabled>false</smartLocatorEnabled>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//a[contains(.,'Semua Produk')]</value>
      <webElementGuid>28d84d1b-e248-491b-bca7-9fa14149363d</webElementGuid>
   </webElementProperties>
</WebElementEntity>
