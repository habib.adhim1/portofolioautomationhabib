<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>dropdownKategori</name>
   <tag></tag>
   <elementGuidId>ee2c9011-1ae0-45c2-aa4c-15acc1b21293</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'product_category_id']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <smartLocatorEnabled>false</smartLocatorEnabled>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>product_category_id</value>
      <webElementGuid>5e126e82-1ef3-4092-8816-8ec94297567d</webElementGuid>
   </webElementProperties>
</WebElementEntity>
