<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btnUploadFotoProduk</name>
   <tag></tag>
   <elementGuidId>48c994e6-5db0-443e-b07f-681f90ba84e0</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'product_images']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <smartLocatorEnabled>false</smartLocatorEnabled>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>product_images</value>
      <webElementGuid>e982575e-e36c-425c-9898-3e0bff7f184e</webElementGuid>
   </webElementProperties>
</WebElementEntity>
