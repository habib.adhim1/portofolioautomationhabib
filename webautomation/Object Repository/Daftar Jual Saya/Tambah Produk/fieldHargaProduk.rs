<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>fieldHargaProduk</name>
   <tag></tag>
   <elementGuidId>14e6e4bf-4b02-446a-8547-410ac0cd48a0</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'product_price']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <smartLocatorEnabled>false</smartLocatorEnabled>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>product_price</value>
      <webElementGuid>358d4872-0992-4892-bcc9-7dc0286eba7e</webElementGuid>
   </webElementProperties>
</WebElementEntity>
