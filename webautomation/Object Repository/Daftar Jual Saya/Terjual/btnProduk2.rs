<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btnProduk2</name>
   <tag></tag>
   <elementGuidId>cb1ff7a3-e5e8-4fc8-8b1e-02608dac4555</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//div[@class='row g-4']//a[@href='/products/102131']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <smartLocatorEnabled>false</smartLocatorEnabled>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//div[@class='row g-4']//a[@href='/products/102131']</value>
      <webElementGuid>1cc21af7-d54f-4f23-bef5-2dbe883878ec</webElementGuid>
   </webElementProperties>
</WebElementEntity>
