@EditProduk
Feature: Edit produk


  @EP001
  Scenario: i want to edit product selling as a seller user
  	Given user login as seller
    And user click icon sandwich
    And user click card product buyer
    And user click button edit card
    And user set nama produk
    And user set harga produk
    And user set dropdown kategori
    And user set deskripsi
    And user click icon delete image
    And user click button preview
    And user click carousel control next
    And user click carousel control prev
    And user click icon carousel control indicator
    And user click button terbitkan preview
    And user click icon sandwich twice
    Then user back to Home

  