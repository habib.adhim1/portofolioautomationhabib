@Homepage
Feature: homepage


	@HO001
	Scenario: Success checking all homepage features
		Given user landing on homepage
		And user click icon profile
		And user click notification
		And user click sandwich
		And user click home
		And user scroll down
		And user click next
		And user scroll down
		And user click previous
		And user search product
		And user click hobi category
		And user success click a category
		And user success click product hobi
		And user success click a product
		And user click back
		And user scroll up
		And user click kendaraan category
		And user success click a category
		And user click product kendaraan
		And user success click a product
		And user click back
		And user scroll up
		And user click baju category
		And user success click a category
		And user click product baju
		And user success click a product
		And user click back
		And user scroll up
		And user click elektronik category
		And user success click a category
		And user click product elektronik
		And user success click a product
		And user click back
		And user scroll up
		And user click kesehatan category
		And user success click a category
		And user click product kesehatan
		And user success click a product
		And user click back
		And user scroll up
		And user click semua category
		Then user success click a category
