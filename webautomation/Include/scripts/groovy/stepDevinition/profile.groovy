package stepDevinition
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class profile {
	@Given("user success landing on homepage")
	public void user_success_landing_on_homepage() {
		WebUI.callTestCase(findTestCase('pageProfile/testProfile/User landing on homepage'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("user upload foto profile")
	public void user_upload_foto_profile() {
		WebUI.callTestCase(findTestCase('pageProfile/testProfile/User upload foto profile'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@And("user input username")
	public void user_input_username() {
		WebUI.callTestCase(findTestCase('pageProfile/testProfile/User input username'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@And("user click dropdown kota")
	public void user_click_dropdown_kota() {
		WebUI.callTestCase(findTestCase('pageProfile/testProfile/User click dropdown kota'), [('jogjaSelect') : 'Jogja'], FailureHandling.STOP_ON_FAILURE)
	}

	@And("user click input alamat")
	public void user_click_input_alamat() {
		WebUI.callTestCase(findTestCase('pageProfile/testProfile/User input alamat'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@And("user input phone number")
	public void user_input_phone_number() {
		WebUI.callTestCase(findTestCase('pageProfile/testProfile/User input phone number'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@And("user click simpan button")
	public void user_click_simpan_button() {
		WebUI.callTestCase(findTestCase('pageProfile/testProfile/User click simpan button'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("user success change profile info")
	public void user_success_change_profile_info() {
		WebUI.callTestCase(findTestCase('pageProfile/testProfile/User success change profile info'), [:], FailureHandling.STOP_ON_FAILURE)
	}
}