package stepDevinition
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class Notification {
	@Given("user open browser website notification")
	public void user_open_browser_website_notification( ) {
		WebUI.callTestCase(findTestCase('pageLogin/testLogin/testOpenBrowser'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	@When("user before landing user input url website notification")
	public void user_before_landing_user_input_url_website_notification( ) {
		WebUI.callTestCase(findTestCase('pageLogin/testLogin/testNavigateToUrl'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	@And("user make maximize screen notification")
	public void user_make_maximize_screen_notification( ) {
		WebUI.callTestCase(findTestCase('pageLogin/testLogin/testMaximizeWindow'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	@And("user click button masuk login notification")
	public void user_click_button_masuk_login_notification( ) {
		WebUI.callTestCase(findTestCase('pageRegister/testRegister/testBtnMasukLogin (1)'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	@And("user input email notification")
	public void user_input_email_notification( ) {
		WebUI.callTestCase(findTestCase('pageLogin/testLogin/testSetEmailSeller'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	@And("user input password notification")
	public void user_input_password_notification( ) {
		WebUI.callTestCase(findTestCase('pageLogin/testLogin/testSetPassword'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	@And("user click button masuk notification")
	public void user_click_button_masuk_notification( ) {
		WebUI.callTestCase(findTestCase('pageLogin/testLogin/testBtnLogin'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	@And("user click icon notification")
	public void user_click_icon_notification( ) {
		WebUI.callTestCase(findTestCase('pageNotification/testNotification/testIconNotification'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	@And("user click the product notification")
	public void user_click_the_product_notification( ) {
		WebUI.callTestCase(findTestCase('pageNotification/testNotification/testImageNotification'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	@And("user click button back to home page")
	public void user_click_button_back_to_home_page( ) {
		WebUI.callTestCase(findTestCase('pageNotification/testNotification/testBtnBackHome'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	@Then("user success see notification")
	public void user_success_see_notification( ) {
		WebUI.callTestCase(findTestCase('pageLogout/testLogout/testUserCloseBrowse'), [:], FailureHandling.STOP_ON_FAILURE)
	}
}

