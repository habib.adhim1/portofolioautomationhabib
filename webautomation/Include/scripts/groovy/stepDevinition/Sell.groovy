package stepDevinition
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class Sell {
	@Given("user landing on sellpage")
	public void user_landing_on_sellpage( ) {
		WebUI.callTestCase(findTestCase('pageSell/Pages/User landing on sellpage'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	@And("user input product name")
	public void user_input_product_name( ) {
		WebUI.callTestCase(findTestCase('pageSell/Pages/User input product name'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	@And("user input product price")
	public void user_input_product_price( ) {
		WebUI.callTestCase(findTestCase('pageSell/Pages/User input product price'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	@And("user input category")
	public void user_input_category( ) {
		WebUI.callTestCase(findTestCase('pageSell/Pages/User input category'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	@And("user input description")
	public void user_input_description( ) {
		WebUI.callTestCase(findTestCase('pageSell/Pages/User input description'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	@And("user upload image")
	public void user_upload_image( ) {
		WebUI.callTestCase(findTestCase('pageSell/Pages/User upload image'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	@And("user click preview")
	public void user_click_preview( ) {
		WebUI.callTestCase(findTestCase('pageSell/Pages/User click preview'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	@And("user success go to preview page")
	public void user_success_go_to_preview_page( ) {
		WebUI.callTestCase(findTestCase('pageSell/Pages/User success go to preview page'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	@And("user click terbitkan")
	public void user_click_terbitkan( ) {
		WebUI.callTestCase(findTestCase('pageSell/Pages/User click terbitkan'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	@And("user click homee")
	public void user_click_homee( ) {
		WebUI.callTestCase(findTestCase('pageHome/Pages/User click home'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	@Then("user successfully sell a product")
	public void user_successfully_sell_a_product( ) {
		WebUI.callTestCase(findTestCase('pageSell/Pages/User successfully sell a product'), [:], FailureHandling.STOP_ON_FAILURE)
	}
}