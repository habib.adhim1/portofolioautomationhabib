package stepDevinition
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class Register {
	@Given("user open browser website register")
	public void user_open_browser_website_register( ) {
		WebUI.callTestCase(findTestCase('pageLogin/testLogin/testOpenBrowser'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	@When("user before landing user input url website register")
	public void user_before_landing_user_input_url_website_register( ) {
		WebUI.callTestCase(findTestCase('pageLogin/testLogin/testNavigateToUrl'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	@And("user make maximize screen register")
	public void user_make_maximize_screen_register( ) {
		WebUI.callTestCase(findTestCase('pageLogin/testLogin/testMaximizeWindow'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	@And("user landing to homepage click button masuk login page")
	public void user_landing_to_homepage_click_button_masuk_login_page( ) {
		WebUI.callTestCase(findTestCase('pageRegister/testRegister/testBtnMasukLogin'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	@And("user click button daftar di sini")
	public void  user_click_button_daftar_di_sini( ) {
		WebUI.callTestCase(findTestCase('pageRegister/testRegister/testBtnTextRegister'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	@And("user after check button daftar di sini back to login register using button masuk di sini")
	public void user_after_check_button_daftar_di_sini_back_to_login_register_using_button_masuk_di_sini( ) {
		WebUI.callTestCase(findTestCase('pageRegister/testRegister/testBtnBackToLogin'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	@And("user click back button daftar di sini for register")
	public void  user_click_back_button_daftar_di_sini_for_register( ) {
		WebUI.callTestCase(findTestCase('pageRegister/testRegister/testBtnTextRegister'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	@And("user input name register")
	public void user_input_name_register( ) {
		WebUI.callTestCase(findTestCase('pageRegister/testRegister/testUserName'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	@And("user input email register")
	public void user_input_email_register( ) {
		WebUI.callTestCase(findTestCase('pageRegister/testRegister/testSetEmail'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	@And("user input password register")
	public void user_inpur_password_register( ) {
		WebUI.callTestCase(findTestCase('pageRegister/testRegister/testSetPassword'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	@And("user click button daftar register")
	public void user_click_button_daftar_register( ) {
		WebUI.callTestCase(findTestCase('pageRegister/testRegister/testBtnDaftar'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	@And("user want to see all the product list scroll down")
	public void user_want_to_see_all_the_product_list_scroll_down( ) {
		WebUI.callTestCase(findTestCase('pageRegister/testRegister/testScrollDown'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	@And("user want to see all the product list scroll up")
	public void user_want_to_see_all_the_product_list_scroll_up( ) {
		WebUI.callTestCase(findTestCase('pageRegister/testRegister/TestScrollUp'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	@Then("user success register")
	public void user_success_register( ) {
		WebUI.callTestCase(findTestCase('pageRegister/testRegister/testCloseBrowser'), [:], FailureHandling.STOP_ON_FAILURE)
	}
}
