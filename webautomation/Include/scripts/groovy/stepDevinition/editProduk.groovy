package stepDevinition
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class editProduk {

	@Given("user login as seller")
	public void user_login_as_seller( ) {
		WebUI.callTestCase(findTestCase('pageEditProduct/Page/user login as a seller'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@And("user click icon sandwich")
	public void user_click_icon_sandwich( ) {
		WebUI.callTestCase(findTestCase('pageEditProduct/Page/user click icon sandwich'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	
	@And("user click card product buyer")
	public void user_click_card_product_buyer( ) {
		WebUI.callTestCase(findTestCase('pageEditProduct/Page/user click card product'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	
	@And("user click button edit card")
	public void user_click_button_edit_card( ) {
		WebUI.callTestCase(findTestCase('pageEditProduct/Page/user click button edit'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	
	@And("user set nama produk")
	public void user_set_nama_produk( ) {
		WebUI.callTestCase(findTestCase('pageEditProduct/Page/user set nama produk'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	
	@And("user set harga produk")
	public void user_set_harga_produk( ) {
		WebUI.callTestCase(findTestCase('pageEditProduct/Page/user set harga produk'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	
	@And("user set dropdown kategori")
	public void user_set_dropdown_kategori( ) {
		WebUI.callTestCase(findTestCase('pageEditProduct/Page/user set dropdown kategori'), [('variable') : 'Hobi'], FailureHandling.STOP_ON_FAILURE)
	}
	
	@And("user set deskripsi")
	public void user_set_deskripsi( ) {
		WebUI.callTestCase(findTestCase('pageEditProduct/Page/user set deskripsi'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	
	@And("user click icon delete image")
	public void user_click_icon_delete_image( ) {
		WebUI.callTestCase(findTestCase('pageEditProduct/Page/user click icon delete image'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	
	@And("user click button preview")
	public void user_click_button_preview( ) {
		WebUI.callTestCase(findTestCase('pageEditProduct/Page/user click button preview'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	
	@And("user click carousel control next")
	public void user_click_carousel_control_next( ) {
		WebUI.callTestCase(findTestCase('pageEditProduct/Page/user click carousel control next'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	
	@And("user click carousel control prev")
	public void user_click_carousel_control_prev( ) {
		WebUI.callTestCase(findTestCase('pageEditProduct/Page/user click carousel control prev'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	
	@And("user click icon carousel control indicator")
	public void  user_click_icon_carousel_control_indicator( ) {
		WebUI.callTestCase(findTestCase('pageEditProduct/Page/user click icon carousel indicator'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	
	@And("user click button terbitkan preview")
	public void user_click_button_terbitkan_preview( ) {
		WebUI.callTestCase(findTestCase('pageEditProduct/Page/user click button terbitkan preview'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	
	@And("user click icon sandwich twice")
	public void user_click_icon_sandwich_twice( ) {
		WebUI.callTestCase(findTestCase('pageEditProduct/Page/user click icon sandwich'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("user back to Home")
	public void user_back_to_Home( ) {
		WebUI.callTestCase(findTestCase('pageEditProduct/Page/user back to Homepage'), [:], FailureHandling.STOP_ON_FAILURE)
	}
}