package stepDevinition
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When


class beliProduk {

	@Given("user login as buyer")
	public void user_login_as_buyer( ) {
		WebUI.callTestCase(findTestCase('pageBeliProduct/Page/user login as buyer'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	@And("user search produk")
	public void user_search_produk( ) {
		WebUI.callTestCase(findTestCase('pageBeliProduct/Page/user search produk'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	@And("testScrollDown")
	public void testScrollDown( ) {
		WebUI.callTestCase(findTestCase('pageRegister/testRegister/testScrollDown'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	@And("user click card product")
	public void user_click_card_product( ) {
		WebUI.callTestCase(findTestCase('pageBeliProduct/Page/user click card product'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	@And("user click button SayaTertarikdanNego")
	public void uuser_click_button_SayaTertarikdanNego( ) {
		WebUI.callTestCase(findTestCase('pageBeliProduct/Page/user click button sayaTertarikdanNego'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	@And("user set harga tawar")
	public void user_set_harga_tawar( ) {
		WebUI.callTestCase(findTestCase('pageBeliProduct/Page/user set harga tawar'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	@And("user click button kirim")
	public void user_click_button_kirim( ) {
		WebUI.callTestCase(findTestCase('pageBeliProduct/Page/user click button kirim'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	@Then("user back to Homepage")
	public void user_back_to_Homepage( ) {
		WebUI.callTestCase(findTestCase('pageEditProduct/Page/user back to Homepage'), [:], FailureHandling.STOP_ON_FAILURE)
	}
}