package stepDevinition
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When


class Logout {

	@Given("user open browser website logout")
	public void user_open_browser_website_logout( ) {
		WebUI.callTestCase(findTestCase('pageLogin/testLogin/testOpenBrowser'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	@When("before landing user input url website logout")
	public void before_landing_user_input_url_website_logout( ) {
		WebUI.callTestCase(findTestCase('pageLogin/testLogin/testNavigateToUrl'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	@And("user make maximize screen logout")
	public void user_make_maximize_screen_logout( ) {
		WebUI.callTestCase(findTestCase('pageLogin/testLogin/testMaximizeWindow'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	@And("user landing to homepage click button masuk login")
	public void user_landing_on_homepage_click_button_masuk_login( ) {
		WebUI.callTestCase(findTestCase('pageRegister/testRegister/testBtnMasukLogin (1)'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	@And("user want to login user input email logout")
	public void user_want_to_login_user_input_email_logout( ) {
		WebUI.callTestCase(findTestCase('pageLogin/testLogin/testSetEmailBuyer'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	@And("user input password logout")
	public void user_input_password_logout( ) {
		WebUI.callTestCase(findTestCase('pageLogin/testLogin/testSetPassword'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	@And("user click button masuk logout")
	public void user_click_button_masuk_logout( ) {
		WebUI.callTestCase(findTestCase('pageLogin/testLogin/testBtnLogin'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	@And("user click icon profile account logout")
	public void user_click_icon_profile_account_logout( ) {
		WebUI.callTestCase(findTestCase('pageLogin/testLogin/testIconProfile'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	@And("user check account profile logout")
	public void user_check_account_profile_logout( ) {
		WebUI.callTestCase(findTestCase('pageLogin/testLogin/testUserVerifyProfileBuy'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	@And("user success logout")
	public void user_success_logout( ) {
		WebUI.callTestCase(findTestCase('pageLogout/testLogout/testBtnLogout'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	@Then("user close browser logout")
	public void user_close_browser_logout( ) {
		WebUI.callTestCase(findTestCase('pageLogout/testLogout/testUserCloseBrowse'), [:], FailureHandling.STOP_ON_FAILURE)
	}
}

