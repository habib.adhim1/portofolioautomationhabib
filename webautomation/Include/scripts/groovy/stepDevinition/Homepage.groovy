package stepDevinition
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When

//homepage aman

class Homepage {
	@Given("user landing on homepage")
	public void user_landing_on_homepage( ) {
		WebUI.callTestCase(findTestCase('pageHome/Pages/User landing on homepage'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	@And("user click icon profile")
	public void user_click_icon_profile( ) {
		WebUI.callTestCase(findTestCase('pageHome/Pages/User click icon profile'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	@And("user click notification")
	public void user_click_notification( ) {
		WebUI.callTestCase(findTestCase('pageHome/Pages/User click notification'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	@And("user click sandwich")
	public void user_click_sandwich( ) {
		WebUI.callTestCase(findTestCase('pageHome/Pages/User click sandwich'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	@And("user click home")
	public void user_click_home( ) {
		WebUI.callTestCase(findTestCase('pageHome/Pages/User click home'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	@And("user scroll down")
	public void user_scroll_down( ) {
		WebUI.callTestCase(findTestCase('pageHome/Pages/User scroll down'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	@And("user click next")
	public void user_click_next( ) {
		WebUI.callTestCase(findTestCase('pageHome/Pages/User click next'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	@And("user click previous")
	public void user_click_previous( ) {
		WebUI.callTestCase(findTestCase('pageHome/Pages/User click previous'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	@And("user search product")
	public void user_search_product( ) {
		WebUI.callTestCase(findTestCase('pageHome/Pages/User search product'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	@And("user click hobi category")
	public void user_click_hobi_category( ) {
		WebUI.callTestCase(findTestCase('pageHome/Pages/User click hobi category'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	@And("user success click a category")
	public void user_success_click_a_category( ) {
		WebUI.callTestCase(findTestCase('pageHome/Pages/User success click a category'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	@And("user success click product hobi")
	public void user_click_product_hobi( ) {
		WebUI.callTestCase(findTestCase('pageHome/Pages/User click product hobi'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	@And("user success click a product")
	public void user_success_click_a_product( ) {
		WebUI.callTestCase(findTestCase('pageHome/Pages/User success click a product'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	@And("user click back")
	public void user_click_back( ) {
		WebUI.callTestCase(findTestCase('pageHome/Pages/User click back'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	@And("user scroll up")
	public void user_scroll_up( ) {
		WebUI.callTestCase(findTestCase('pageHome/Pages/User scroll up'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	@And("user click kendaraan category")
	public void user_click_kendaraan_category( ) {
		WebUI.callTestCase(findTestCase('pageHome/Pages/User click kendaraan category'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	@And("user click product kendaraan")
	public void user_click_product_kendaraan( ) {
		WebUI.callTestCase(findTestCase('pageHome/Pages/User click product kendaraan'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	@And("user click baju category")
	public void user_click_baju_category( ) {
		WebUI.callTestCase(findTestCase('pageHome/Pages/User click baju category'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	@And("user click product baju")
	public void user_click_product_baju( ) {
		WebUI.callTestCase(findTestCase('pageHome/Pages/User click product baju'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	@And("user click elektronik category")
	public void user_click_elektronik_category( ) {
		WebUI.callTestCase(findTestCase('pageHome/Pages/User click elektronik category'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	@And("user click product elektronik")
	public void user_click_product_elektronik( ) {
		WebUI.callTestCase(findTestCase('pageHome/Pages/User click product elektronik'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	@And("user click kesehatan category")
	public void user_click_kesehatan_category( ) {
		WebUI.callTestCase(findTestCase('pageHome/Pages/User click kesehatan category'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	@And("user click product kesehatan")
	public void user_click_product_kesehatan( ) {
		WebUI.callTestCase(findTestCase('pageHome/Pages/User click product kesehatan'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	@And("user click semua category")
	public void user_click_semua_category( ) {
		WebUI.callTestCase(findTestCase('pageHome/Pages/User click semua category'), [:], FailureHandling.STOP_ON_FAILURE)
	}
}