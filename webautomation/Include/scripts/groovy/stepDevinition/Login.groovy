package stepDevinition
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When


////////////////////////////LOGN BUYER//////////////////////////////////////////////////////////////////////

class Login {
	@Given("user open browser website buyer")
	public void user_open_browser_website_buyer( ) {
		WebUI.callTestCase(findTestCase('pageLogin/testLogin/testOpenBrowser'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	@When("user before landing user input url website buyer")
	public void user_before_landing_user_input_url_website_buyer( ) {
		WebUI.callTestCase(findTestCase('pageLogin/testLogin/testNavigateToUrl'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	@And("user make maximize screen buyer")
	public void user_make_maximize_screen_buyer( ) {
		WebUI.callTestCase(findTestCase('pageLogin/testLogin/testMaximizeWindow'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	@And("user landing to homepage click button masuk buyer")
	public void user_landing_to_homepage_click_button_masuk_buyer( ) {
		WebUI.callTestCase(findTestCase('pageLogin/testLogin/testBtnMasukLogin'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	@And("user input email buyer")
	public void user_input_email_buyer( ) {
		WebUI.callTestCase(findTestCase('pageLogin/testLogin/testSetEmailBuyer'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	@And("user input password buyer")
	public void user_input_password_buyer( ) {
		WebUI.callTestCase(findTestCase('pageLogin/testLogin/testSetPassword'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	@And("user click button masuk buyer")
	public void user_click_button_masuk_buyer( ) {
		WebUI.callTestCase(findTestCase('pageLogin/testLogin/testBtnLogin'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	@And("user check the profile account name as buyer")
	public void user_check_the_profile_account_name_as_buyer( ) {
		WebUI.callTestCase(findTestCase('pageLogin/testLogin/testIconProfile'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	@And("user verify account buyer")
	public void user_verify_account_buyer( ) {
		WebUI.callTestCase(findTestCase('pageLogin/testLogin/testUserVerifyProfileBuy'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	@And("user success login buyer and want to logout and want to change login seller")
	public void user_success_login_buyer_and_want_to_logout_and_want_to_change_login_seller( ) {
		WebUI.callTestCase(findTestCase('pageLogout/testLogout/testBtnLogout'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	@Then("user close browser buyer")
	public void user_close_browser_buyer( ) {
		WebUI.callTestCase(findTestCase('pageLogout/testLogout/testUserCloseBrowse'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	////////////////////////////LOGN SELLER//////////////////////////////////////////////////////////////////////

	@Given("user open browser website seller")
	public void user_open_browser_website_seller( ) {
		WebUI.callTestCase(findTestCase('pageLogin/testLogin/testOpenBrowser'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	@When("user before landing user input url website seller")
	public void before_landing_user_input_url_website_seller( ) {
		WebUI.callTestCase(findTestCase('pageLogin/testLogin/testNavigateToUrl'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	@And("user make maximize screen seller")
	public void user_make_maximize_screen_seller( ) {
		WebUI.callTestCase(findTestCase('pageLogin/testLogin/testMaximizeWindow'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	@And("user landing to homepage click button masuk seller")
	public void user_landing_to_homepage_click_button_masuk_seller( ) {
		WebUI.callTestCase(findTestCase('pageLogin/testLogin/testBtnMasukLogin'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	@And("user input email seller")
	public void user_input_email_seller( ) {
		WebUI.callTestCase(findTestCase('pageLogin/testLogin/testSetEmailSeller'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	@And("user input password seller")
	public void user_input_password_seller( ) {
		WebUI.callTestCase(findTestCase('pageLogin/testLogin/testSetPassword'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	@And("user click button login seller")
	public void user_click_button_login_seller( ) {
		WebUI.callTestCase(findTestCase('pageLogin/testLogin/testBtnLogin'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	@And("user check the profile account name as seller")
	public void user_check_the_profile_account_name_as_seller( ) {
		WebUI.callTestCase(findTestCase('pageLogin/testLogin/testIconProfile'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	@And("user verify account seller")
	public void user_verify_account_seller( ) {
		WebUI.callTestCase(findTestCase('pageLogin/testLogin/testUserVerifyProfileSell'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	@And("user success login seller and want to logout")
	public void user_success_login_seller_and_want_to_logout( ) {
		WebUI.callTestCase(findTestCase('pageLogout/testLogout/testBtnLogout'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	@Then("user close browser seller")
	public void user_close_browser_seller( ) {
		WebUI.callTestCase(findTestCase('pageLogout/testLogout/testUserCloseBrowse'), [:], FailureHandling.STOP_ON_FAILURE)
	}
}

