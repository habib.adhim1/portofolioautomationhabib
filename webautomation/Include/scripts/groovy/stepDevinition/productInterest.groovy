package stepDevinition
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import java.security.PublicKey

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class productInterest {
	@Given("user landing on interest product page")
	public void user_landing_on_interest_product_page() {
		WebUI.callTestCase(findTestCase('pageDaftarJualSaya/User as seller want to see the products are interested/Page interest product/User landing on interested product page'),
				[:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("user click produk interested1")
	public void user_click_produk_interested1() {
		WebUI.callTestCase(findTestCase('pageDaftarJualSaya/User as seller want to see the products are interested/Page interest product/User click product1'),
				[:], FailureHandling.STOP_ON_FAILURE)
	}

	@And("user click produk interested2")
	public void user_click_produk_interested2() {
		WebUI.callTestCase(findTestCase('pageDaftarJualSaya/User as seller want to see the products are interested/Page interest product/User click product2'),
				[:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("user success see the product are interested")
	public void user_success_see_the_product_are_interested() {
		WebUI.callTestCase(findTestCase('pageDaftarJualSaya/User as seller want to see the products are interested/Page interest product/User success see the products are interested'),
				[:], FailureHandling.STOP_ON_FAILURE)
	}
}